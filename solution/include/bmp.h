//
// Created by europ on 27.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_MASTER_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_MASTER_BMP_H
#include "image.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};

static const char *const read_status_msg[] = {
        [READ_OK] = "Read from BMP file was successful",
        [READ_INVALID_SIGNATURE] = "Read failed, invalid signature",
        [READ_INVALID_BITS] = "Read failed, invalid bits",
        [READ_INVALID_HEADER] = "Read failed, invalid header",
};

enum read_status from_bmp(FILE *file, struct image* image);

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

static const char *const write_status_msg[] = {
        [WRITE_OK] = "Write to BMP file was successful",
        [WRITE_ERROR] = "Write failed",

};

enum write_status to_bmp(FILE *f, struct image* image);

void print_header(struct bmp_header header);

#endif //ASSIGNMENT_IMAGE_ROTATION_MASTER_BMP_H
