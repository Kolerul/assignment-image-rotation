//
// Created by europ on 27.12.2021.
//

#ifndef UNTITLED3_IMAGE_H
#define UNTITLED3_IMAGE_H
#include <inttypes.h>

#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel** data;
};

#endif //UNTITLED3_IMAGE_H
