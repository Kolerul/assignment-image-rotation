//
// Created by europ on 27.12.2021.
//

#ifndef UNTITLED3_UTIL_H
#define UNTITLED3_UTIL_H
#include "image.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>


void marray_free(struct pixel** marray, uint64_t height);

void print_marray(struct image image);

//enum open_file_status openFile(FILE** f, char* argv, char* mode);

#endif //UNTITLED3_UTIL_H
