//
// Created by europ on 25.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION1_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION1_FILE_H
#include "bmp.h"

enum open_file_status  {
    OPEN_OK = 0,
    OPEN_FAIL
};

static const char *const file_status_msg[] = {
        [OPEN_OK] = "File was open successful",
        [OPEN_FAIL] = "Open failed"
};

enum open_file_status openFile(FILE** f, char* argv, char* mode);

int64_t read_image(char** argv, struct image* image, enum read_status (f)(FILE*, struct image*));

int64_t write_image(char** argv, struct image* image, enum write_status (f)(FILE*, struct image*));

#endif //ASSIGNMENT_IMAGE_ROTATION1_FILE_H
