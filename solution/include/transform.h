//
// Created by europ on 27.12.2021.
//

#ifndef UNTITLED3_TRANSFORM_H
#define UNTITLED3_TRANSFORM_H
#include "image.h"
#include <stdlib.h>

struct image rotate( struct image const source );

struct image image_transform(struct image const old, struct image (f)( struct image const));

#endif //UNTITLED3_TRANSFORM_H
