//
// Created by europ on 27.12.2021.
//

#include "../include/bmp.h"
#define TYPE 19778
#define OFFBITS 54
#define HEADERSIZE 40
#define PLANES 1
#define BITCOUNT 24
#define XPIXELSPERMETER 2834
#define YPIXELSPERMETER 2834


static int32_t padding_counter(uint32_t width){
    int32_t padding = 4 - ((width * 3) % 4);
    return padding;
}

static struct pixel* array_creater(uint32_t width, FILE* f){
    if (width <= 0) {
        return NULL;
    }
    struct pixel *array = malloc(sizeof(struct pixel) * width);
    array[0] = (struct pixel) {0};

    for (int32_t i = 0; i < width; i++){
        fread(array+i, sizeof(struct pixel), 1, f);
    }
    int32_t pad = padding_counter(width);
    char padding[pad];
    size_t report1 = fread(padding, sizeof(char), pad, f);
    if (report1 != pad){
        //printf("Something went wrong");
        return NULL;
    }
    return array;
}

static struct pixel** marray_creater(uint32_t height, uint32_t width, FILE* f){
    struct pixel** marray = NULL;
    if (height > 0){
        marray = (struct pixel**) malloc(sizeof(struct pixel*) * height);
        for (int32_t i = height - 1; i >= 0; i--){
            marray[i] = array_creater(width, f);
        }
    }
    return marray;
}

enum read_status from_bmp(FILE *file, struct image* image){
    struct bmp_header header = {0};
    struct pixel** pixel_array;
    size_t report1 = fread(&header, sizeof(header), 1, file);
    if (report1 != 1){
        return READ_INVALID_HEADER;
    }
    if (header.bfType == 19778){
        //print_header(header);
        pixel_array = marray_creater(header.biHeight, header.biWidth, file);

        if (pixel_array == NULL){
            return READ_INVALID_BITS;
        }

    }else{
        return READ_INVALID_HEADER;
    }
    struct image image1 = {.width = header.biWidth, .height = header.biHeight, .data = pixel_array};
    *image = image1;
    return READ_OK;
}



enum write_status to_bmp(FILE *f, struct image* image){
    int32_t padding = padding_counter(image->width);

    struct bmp_header header = {.bfType = TYPE, .bfileSize = OFFBITS + ((image -> width)*(image ->height) * 3 + (image ->height)*padding),
            .bOffBits = OFFBITS, .biSize = HEADERSIZE, .biWidth = image -> width, .biHeight = image -> height, .biPlanes = PLANES,
            .biBitCount = BITCOUNT, .biSizeImage = ((image -> width)*(image ->height) * 3 + (image ->height)*padding),
            .biXPelsPerMeter = XPIXELSPERMETER, .biYPelsPerMeter = YPIXELSPERMETER};

    if( fwrite(&header, sizeof(header), 1, f) != 1){
        return WRITE_ERROR;
    }
    for (int64_t i = image->height - 1; i >= 0; i--){

        for (int64_t j = 0; j < image->width; j++){
            size_t report1 = fwrite(&(image->data[i][j]), sizeof(struct pixel), 1, f);
            if (report1 != 1){
                return WRITE_ERROR;
            }
        }
        if(padding != 0){
            fseek(f, padding, SEEK_CUR);
        }

    }
    return WRITE_OK;
}

void print_header(struct bmp_header header){
    printf("Type %" PRIu16 "\n", header.bfType);
    printf("fileSize %" PRIu32 "\n", header.bfileSize);
    printf("Reserved %" PRIu32 "\n", header.bfReserved);
    printf("OffBits %" PRIu32 "\n", header.bOffBits);
    printf("Size %" PRIu32 "\n", header.biSize);
    printf("Width %" PRIu32 "\n", header.biWidth);
    printf("Height %" PRIu32 "\n", header.biHeight);
    printf("Planes %" PRIu16 "\n", header.biPlanes);
    printf("BitCount %" PRIu16 "\n", header.biBitCount);
    printf("Compression %" PRIu32 "\n", header.biCompression);
    printf("SizeImage %" PRIu32 "\n", header.biSizeImage);
    printf("XPelsPerMeter %" PRIu32 "\n", header.biXPelsPerMeter);
    printf("YPelsPerMeter %" PRIu32 "\n", header.biYPelsPerMeter);
    printf("ClrUsed %" PRIu32 "\n", header.biClrUsed);
    printf("ClrImportant %" PRIu32 "\n", header.biClrImportant);

}
