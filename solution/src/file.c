//
// Created by europ on 25.01.2022.
//

#include "../include/file.h"



enum open_file_status openFile(FILE** f, char* argv, char* mode){
    *f = fopen(argv, mode);
    if (!*f){
        return OPEN_FAIL;
    }else{
        return OPEN_OK;
    }
}

int64_t read_image(char** argv, struct image* image, enum read_status (f)(FILE*, struct image*)){
    FILE *f1 = NULL;
    enum open_file_status report = openFile(&f1, argv[1], "rb");
    printf("%s \n", file_status_msg[report]);
    if(report == OPEN_FAIL){
        return 1;
    }else{
        enum read_status readStatus = f(f1, image);
        printf("%s \n", read_status_msg[readStatus]);
        if (readStatus != READ_OK){
            return 1;
        }
        fclose(f1);
    }
    return 0;
}

int64_t write_image(char** argv, struct image* image, enum write_status (f)(FILE*, struct image*)){
    FILE *f2 = NULL;
    enum open_file_status report = openFile(&f2, argv[2], "wb");
    printf("%s \n", file_status_msg[report]);
    if(report == OPEN_FAIL){
        return 1;
    }else{
        enum write_status writeStatus = f(f2, image);
        printf("%s \n",write_status_msg[writeStatus]);
        if (writeStatus != WRITE_OK){
            return 1;
        }
        fclose(f2);
    }
    return 0;
}
