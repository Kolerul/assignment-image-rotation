#include "../include/bmp.h"
#include "../include/file.h"
#include "../include/transform.h"
#include "../include/util.h"
#include <stdio.h>



int main( int argc, char** argv ) {
    if (argc != 3){
        printf("Wrong number of arguments \n");
        return 1;
    }
    struct image old;
    struct image new;
    int64_t result = read_image(argv, &old, from_bmp);
    if (result == 1){
        return 1;
    }

    new = image_transform(old, rotate);

    result = write_image(argv, &new, to_bmp);
    if (result == 1){
        return 1;
    }


    marray_free(old.data, old.height);
    marray_free(new.data, new.height);

    return 0;
}

