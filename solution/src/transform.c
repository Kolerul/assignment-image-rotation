//
// Created by europ on 27.12.2021.
//

#include "../include/transform.h"

static struct pixel** marray_turn(struct image origin){
    struct pixel** marray = NULL;
    if (origin.width > 0 && origin.height > 0){
        marray = (struct pixel**) malloc(sizeof(struct pixel*) * origin.width);
        for (int32_t i = 0; i < origin.width; i++){
            struct pixel* array = malloc(sizeof(struct pixel) * origin.height);
            for (int32_t j = 0; j < origin.height; j++){
                array[j] = origin.data[j][origin.width - i - 1];
            }
            marray[i] = array;
        }
    }
    return marray;
}

struct image rotate( struct image const source ){
    struct pixel** pixel_array = marray_turn(source);
    return (struct image){.height = source.width, .width = source.height, .data = pixel_array};

}

struct image image_transform(struct image const old, struct image (f)( struct image const)){
    struct image new;
    new = f(old);
    return new;
}
