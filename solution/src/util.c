//
// Created by europ on 27.12.2021.
//

#include "../include/util.h"

void marray_free(struct pixel** marray, uint64_t height){
    for (uint64_t i = 0; i < height; i++){
        free(marray[i]);
    }
    free(marray);
}

void print_marray(struct image image){
    for (int64_t i = 0; i < image.height; i++){

        for (int64_t j = 0; j < image.width; j++){
            printf("[%"PRIu8" %"PRIu8" %"PRIu8"] ", image.data[i][j].b, image.data[i][j].g, image.data[i][j].r);
        }
        printf(";\n");
    }
}

